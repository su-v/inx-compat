#!/usr/bin/env python
'''
adjust_old_files.py - adjust attributes and properties in old files
for Inkscape 0.91

Tool for Inkscape 0.91 to adjust rendering of drawings with linked
or embedded bitmap images created with older versions of Inkscape
or third-party applications. Also includes fix for missing filter
property of Gaussian blurs added via Fill & Stroke dialog.

Copyright (C) 2015-2016, su_v <suv-sf@users.sf.net>


Includes source code from

replace_font.py
Copyright (C) 2010 Craig Marshall, craig9 [at] gmail.com


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''
# pylint: disable=missing-docstring

# standard library
import sys

# compat
import six

# local library
import inkex
import simplestyle

try:
    inkex.localize()
except AttributeError:
    import gettext  # pylint: disable=wrong-import-order
    _ = gettext.gettext


# Globals
TEXT_TAGS = ['{http://www.w3.org/2000/svg}tspan',
             '{http://www.w3.org/2000/svg}text',
             '{http://www.w3.org/2000/svg}flowRoot',
             '{http://www.w3.org/2000/svg}flowPara',
             '{http://www.w3.org/2000/svg}flowSpan']
FONT_ATTRIBUTES = ['font-family', '-inkscape-font-specification']
GENERIC_FONTS = [["Sans", "sans-serif"], ["Serif", "serif"], ["Monospace", "monospace"]]


def set_font(node, new_font, style=None):
    '''
    Sets the font attribute in the style attribute of node, using the
    font name stored in new_font. If the style dict is open already,
    it can be passed in, otherwise it will be optned anyway.

    Returns a dirty boolean flag
    '''
    dirty = False
    if not style:
        style = get_style(node)
    if style:
        for att in FONT_ATTRIBUTES:
            if att in style:
                style[att] = new_font
                set_style(node, style)
                dirty = True
    return dirty


def find_replace_font(node, find, replace):
    '''
    Searches the relevant font attributes/styles of node for find, and
    replaces them with replace.

    Returns a dirty boolean flag
    '''
    dirty = False
    style = get_style(node)
    if style:
        for att in FONT_ATTRIBUTES:
            # if att in style and style[att].strip().lower() == find:
            if att in style and style[att].strip() == find:
                set_font(node, replace, style)
                dirty = True
    return dirty


def is_styled_text(node):
    '''
    Returns true if the tag in question is a "styled" element that
    can hold text.
    '''
    return node.tag in TEXT_TAGS and 'style' in node.attrib


def is_text(node):
    '''
    Returns true if the tag in question is an element that
    can hold text.
    '''
    return node.tag in TEXT_TAGS


def get_style(node):
    '''
    Sugar coated way to get style dict from a node
    '''
    if 'style' in node.attrib:
        return simplestyle.parseStyle(node.attrib['style'])
    else:
        return {}


def set_style(node, style):
    '''
    Sugar coated way to set the style dict, for node
    '''
    node.attrib['style'] = simplestyle.formatStyle(style)


def get_fonts(node):
    '''
    Given a node, returns a list containing all the fonts that
    the node is using.
    '''
    fonts = []
    s = get_style(node)
    if not s:
        return fonts
    for a in FONT_ATTRIBUTES:
        if a in s:
            fonts.append(s[a])
    return fonts


def die(msg="Dying!"):
    inkex.errormsg(msg)
    sys.exit(0)


def report_replacements(num):
    '''
    Sends a message to the end user showing success of failure
    of the font replacement
    '''
    if num == 0:
        die(_('Couldn\'t find anything using that font, ' +
              'please ensure the spelling and spacing is correct.'))


def report_findings(findings):
    '''
    Tells the user which fonts were found, if any
    '''
    if len(findings) == 0:
        inkex.errormsg(_("Didn't find any fonts in this document/selection."))
    else:
        if len(findings) == 1:
            inkex.errormsg(_("Found the following font only: %s") % findings[0])
        else:
            inkex.errormsg(_("Found the following fonts:\n%s") % '\n'.join(findings))


def change_attribute(node, attribute):
    # pylint: disable=too-many-branches
    for key, value in attribute.items():
        if key == 'preserveAspectRatio':
            # set presentation attribute
            if value != "unset":
                node.set(key, str(value))
            else:
                if node.get(key):
                    del node.attrib[key]
        elif key == 'image-rendering':
            node_style = get_style(node)
            if key not in node_style:
                # set presentation attribute
                if value != "unset":
                    node.set(key, str(value))
                else:
                    if node.get(key):
                        del node.attrib[key]
            else:
                # set style property
                if value != "unset":
                    node_style[key] = str(value)
                else:
                    del node_style[key]
                set_style(node, node_style)
        elif key == 'color-interpolation-filters':
            node_style = get_style(node)
            if value == "sRGB_if_missing":
                value = ("sRGB" if key not in node_style else "pass")
            if value == "unset":
                del node_style[key]
            elif value != "pass":
                node_style[key] = str(value)
            set_style(node, node_style)
        elif key == 'font-family' or key == 'generic_fonts':
            pass
        else:
            pass


class SetAttrImage(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        # instance attributes
        self.selected_items = []
        # basic options
        self.OptionParser.add_option("--fix_image_scaling",
                                     action="store", type="inkbool",
                                     dest="fix_image_scaling", default=True,
                                     help="")
        self.OptionParser.add_option("--fix_image_rendering",
                                     action="store", type="inkbool",
                                     dest="fix_image_rendering", default=False,
                                     help="")
        self.OptionParser.add_option("--fix_filter_color_interp",
                                     action="store", type="inkbool",
                                     dest="fix_filter_color_interp", default=True,
                                     help="")
        self.OptionParser.add_option("--fix_generic_fonts",
                                     action="store", type="inkbool",
                                     dest="fix_generic_fonts", default=True,
                                     help="")
        # advanced options
        self.OptionParser.add_option("--image_aspect_ratio",
                                     action="store", type="string",
                                     dest="image_aspect_ratio", default="none",
                                     help="Value for attribute 'preserveAspectRatio'")
        self.OptionParser.add_option("--image_aspect_clip",
                                     action="store", type="string",
                                     dest="image_aspect_clip", default="unset",
                                     help="optional 'meetOrSlice' value")
        self.OptionParser.add_option("--image_aspect_ratio_scope",
                                     action="store", type="string",
                                     dest="image_aspect_ratio_scope", default="selected_only",
                                     help="scope within which to edit " +
                                     "'preserveAspectRatio' attribute")
        self.OptionParser.add_option("--image_rendering",
                                     action="store", type="string",
                                     dest="image_rendering", default="unset",
                                     help="Value for attribute 'image-rendering'")
        self.OptionParser.add_option("--image_rendering_scope",
                                     action="store", type="string",
                                     dest="image_rendering_scope", default="selected_only",
                                     help="scope within which to edit " +
                                     "'image-rendering' attribute")
        self.OptionParser.add_option("--color_interpolation_filters",
                                     action="store", type="string",
                                     dest="color_interpolation_filters", default="sRGB",
                                     help="")
        self.OptionParser.add_option("--color_interpolation_filters_scope",
                                     action="store", type="string",
                                     dest="color_interpolation_filters_scope", default="in_defs",
                                     help="")
        self.OptionParser.add_option("--subst_font_sans_serif",
                                     action="store", type="inkbool",
                                     dest="subst_font_sans_serif", default=True,
                                     help="")
        self.OptionParser.add_option("--subst_font_serif",
                                     action="store", type="inkbool",
                                     dest="subst_font_serif", default=False,
                                     help="")
        self.OptionParser.add_option("--subst_font_monospace",
                                     action="store", type="inkbool",
                                     dest="subst_font_monospace", default=False,
                                     help="")
        self.OptionParser.add_option("--fonts_generic_scope",
                                     action="store", type="string",
                                     dest="fonts_generic_scope", default="in_defs",
                                     help="")
        # tabs
        self.OptionParser.add_option("--tab_main",
                                     action="store", type="string",
                                     dest="tab_main")

    # replace fonts

    def find_child_text_items(self, node):
        '''
        Recursive method for appending all text-type elements
        to self.selected_items
        '''
        if is_text(node):
            self.selected_items.append(node)
            for child in node:
                self.find_child_text_items(child)

    def relevant_items(self, scope):
        '''
        Depending on the scope, returns all text elements, or all
        selected text elements including nested children
        '''
        items = []
        to_return = []
        if scope == "selection_only":
            self.selected_items = []
            for item in six.iteritems(self.selected):
                self.find_child_text_items(item[1])
            items = self.selected_items
            if len(items) == 0:
                die(_("There was nothing selected"))
        else:
            items = self.document.getroot().getiterator()
        to_return.extend(filter(is_text, items))  # pylint: disable=bad-builtin
        return to_return

    def find_replace(self, nodes, find, replace):
        '''
        Walks through nodes, replacing fonts as it goes according
        to find and replace
        '''
        # pylint: disable=no-self-use
        replacements = 0
        for node in nodes:
            if find_replace_font(node, find, replace):
                replacements += 1
        # report_replacements(replacements)

    def replace_all(self, nodes, replace):
        '''
        Walks through nodes, setting fonts indiscriminately.
        '''
        # pylint: disable=no-self-use
        replacements = 0
        for node in nodes:
            if set_font(node, replace):
                replacements += 1
        # report_replacements(replacements)

    def list_all(self, nodes):
        '''
        Walks through nodes, building a list of all fonts found, then
        reports to the user with that list
        '''
        # pylint: disable=no-self-use
        fonts_found = []
        for node in nodes:
            for f in get_fonts(node):
                if f not in fonts_found:
                    fonts_found.append(f)
        report_findings(sorted(fonts_found))

    def replace_listed_fonts(self, font_list, scope="in_document"):
        '''
        Changes listed font names in scope (or in the entire document)
        '''
        relevant_items = self.relevant_items(scope)
        for font in font_list:
            self.find_replace(relevant_items, font[0], font[1])

    # helper

    def get_filter_ref(self, node):
        fprop = 'filter'
        fnode = None
        style = get_style(node)
        if style is not None:
            if fprop in style and style[fprop] != 'none' and style[fprop][:5] == 'url(#':
                filter_id = style[fprop][5:-1]
                # TODO: Catching too general exception Exception (broad-except)
                try:
                    fnode = self.xpathSingle('/svg:svg//svg:filter[@id="%s"]' % filter_id)
                except Exception:  # pylint: disable=broad-except
                    pass
        return fnode

    def change_all(self, node, path, attribute):
        # pylint: disable=no-self-use
        for ele in node.xpath(path, namespaces=inkex.NSS):
            change_attribute(ele, attribute)

    # attribute methods

    def change_selected_only(self, selected, path, attribute):
        if path == './/svg:text':
            self.replace_listed_fonts(attribute['generic_fonts'], "selection_only")
        elif selected:
            for node in selected.values():
                if path == './/svg:image':
                    if node.tag == inkex.addNS('image', 'svg'):
                        change_attribute(node, attribute)
                elif path == './/svg:filter':
                    fnode = self.get_filter_ref(node)
                    if fnode is not None:
                        change_attribute(fnode, attribute)

    def change_in_selection(self, selected, path, attribute):
        if selected:
            for node in selected.values():
                if path == './/svg:image':
                    self.change_all(node, path, attribute)

    def change_in_defs(self, selected, path, attribute):
        # pylint: disable=unused-argument
        node = self.xpathSingle('/svg:svg//svg:defs')
        if node is not None:
            self.change_all(node, path, attribute)

    def change_in_document(self, selected, path, attribute):
        # pylint: disable=unused-argument
        node = self.document.getroot()
        if path == './/svg:text':
            self.replace_listed_fonts(attribute['generic_fonts'])
        else:
            self.change_all(node, path, attribute)

    def change_on_parent_group(self, selected, path, attribute):
        # pylint: disable=no-self-use
        # pylint: disable=unused-argument
        if selected:
            for node in selected.values():
                change_attribute(node.getparent(), attribute)

    def change_on_root_only(self, selected, path, attribute):
        # pylint: disable=unused-argument
        node = self.document.getroot()
        change_attribute(node, attribute)

    def change_basic(self, selected, path, attribute):
        # pylint: disable=unused-argument
        if 'preserveAspectRatio' in attribute:
            self.change_in_document(
                selected, './/svg:image',
                {'preserveAspectRatio': attribute['preserveAspectRatio']})
        if 'image-rendering' in attribute:
            self.change_in_document(
                selected, './/svg:image',
                {'image-rendering': attribute['image-rendering']})
        if 'color-interpolation-filters' in attribute:
            self.change_in_defs(
                selected, './/svg:filter',
                {'color-interpolation-filters': attribute['color-interpolation-filters']})
        if 'generic_fonts' in attribute:
            self.change_in_document(
                selected, './/svg:text',
                {'generic_fonts': attribute['generic_fonts']})

    # main

    def effect(self):

        # init
        path = ''
        attr_val = []
        attr_dict = {}
        cmd_scope = None

        # tabs
        if self.options.tab_main == '"tab_basic"':
            attr_dict['preserveAspectRatio'] = (
                "none" if self.options.fix_image_scaling else "unset")
            attr_dict['image-rendering'] = (
                "optimizeSpeed" if self.options.fix_image_rendering else "unset")
            attr_dict['color-interpolation-filters'] = (
                "sRGB_if_missing" if self.options.fix_filter_color_interp else "pass")
            attr_dict['generic_fonts'] = (
                GENERIC_FONTS if self.options.fix_generic_fonts else [])
            cmd_scope = "basic"
        elif self.options.tab_main == '"tab_image_aspectRatio"':
            path = './/svg:image'
            attr_val = [self.options.image_aspect_ratio]
            if self.options.image_aspect_clip != "unset":
                attr_val.append(self.options.image_aspect_clip)
            attr_dict['preserveAspectRatio'] = ' '.join(attr_val)
            cmd_scope = self.options.image_aspect_ratio_scope
        elif self.options.tab_main == '"tab_image_rendering"':
            path = './/svg:image'
            attr_dict['image-rendering'] = self.options.image_rendering
            cmd_scope = self.options.image_rendering_scope
        elif self.options.tab_main == '"tab_filter_color_interpolation"':
            path = './/svg:filter'
            attr_dict['color-interpolation-filters'] = self.options.color_interpolation_filters
            cmd_scope = self.options.color_interpolation_filters_scope
        elif self.options.tab_main == '"tab_fonts_generic"':
            # pylint: disable=expression-not-assigned
            path = './/svg:text'
            self.options.subst_font_sans_serif and attr_val.append(["Sans", "sans-serif"])
            self.options.subst_font_serif and attr_val.append(["Serif", "serif"])
            self.options.subst_font_monospace and attr_val.append(["Monospace", "monospace"])
            attr_dict['generic_fonts'] = attr_val
            cmd_scope = self.options.fonts_generic_scope
        else:  # help tab
            pass

        # dispatcher
        if cmd_scope is not None:
            try:
                change_cmd = getattr(self, 'change_{0}'.format(cmd_scope))
            except AttributeError:
                inkex.errormsg('Scope "{0}" not supported'.format(cmd_scope))

        # do it
        if change_cmd is not None:
            change_cmd(self.selected, path, attr_dict)


if __name__ == '__main__':
    e = SetAttrImage()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
